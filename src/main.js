const express = require('express')
const { multiply } = require('./mylib')
const app = express()
const port = 3000
const mylib = require('./mylib')


// endpoint localhost:3000/
app.get('/', (req,res) => {
    res.send('Hello World!')
})

// endpoint localhost:3000/add?a=42&b=21
app.get('/add', (req, res) => {
    const a =parseInt(req.query.a);
    const b =parseInt(req.query.b);
    console.log({ a , b })
    const total = mylib.sum(a,b)
    res.send('add works ' + total.toString())
})

//endpoint localhost:3000/multiply?x=3&y=2
app.get('/multiply', (req, res) => {
    const x = parseInt(req.query.x)
    const y = parseInt(req.query.y)
    console.log({ x , y })
    const product = mylib.multiply(x,y)
    res.send('this works too ' + product.toString())
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})