const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');


describe('Unit testing mylib.js', () => {
    
    let myvar = undefined
    before(() => { //before komento ajetaan aina ennen varsinaisia testejä, sen ei tarvitse olla kuitenkaan koodissa ennen niitä
        myvar = 1
    })
    it('Should return 2 when using sum function with a=1 and b=1', () => {
        const result = mylib.sum(1,1)
        expect(result).to.equal(2)
    })

    it('Myvar should exist', () => {
       should.exist(myvar)
    })

    it('Should return 9 when x = 3 and y = 3', () => {
        const result = mylib.multiply(3,3)
        expect(result).to.equal(9)
    })

    it.skip('Assert foo is not bar',  () => {
        assert('foo' !== 'bar')
    })

    after(() => {
        console.log('kukkuu')
    })
})